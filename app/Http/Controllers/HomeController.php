<?php namespace App\Http\Controllers;

use App\User;
use App\Post;
use Auth;
use App\Libraries\TheLoop;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(User $user, Post $post)
	{

        $posts = \TheLoop::getPosts();
        return view('home', compact('posts'));

	}

    public function account(User $user) {

        $user = Auth::user();
        $profile_data = Profile::where('user_id',$user->id)->first();
        $slug = $profile_data->slug;

        return redirect('profile/'.$slug.'/edit');

    }

}
