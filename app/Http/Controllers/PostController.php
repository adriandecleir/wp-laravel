<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
Use Auth;
use Illuminate\Support\Facades\Redirect;


use Illuminate\Http\Request;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Post $post)
	{
        $posts = $post->get();

		return view('admin/posts/index', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('admin/posts/create')->with(array('title' => 'Title', 'slug' => 'Slug', 'content' => 'Content', $add_new = true));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request, Post $post)
	{
        $post->create($request->all());
        return Redirect::action('PostController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug, Post $post)
	{
        $post = $post->where('slug', $slug)->first();
        return view('posts/post', compact('post'));
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($slug, Post $post)
	{

        $post = $post->where('slug', $slug)->first();
        return view('admin/posts/edit', compact('post'))->with(array('title' => $post->title , 'slug' => $post->slug, 'content' => $post->content));

 	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($slug, Request $request, Post $post)
    {

        $post = $post->where('slug',$slug)->first();

        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->save();
        \Session::flash('message', 'Successful Update!');
        return redirect('admin/posts/'.$post->slug.'/edit');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
