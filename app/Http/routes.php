<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::get('admin', [
    'uses' => 'AdminController@index',
    'middleware' => 'admin'
]);


Route::get('admin/posts', [
    'middleware' => 'admin',
    'uses' => 'PostController@index'
]);
Route::get('posts/{slug}', 'PostController@show');
Route::get('admin/posts/{slug}/edit', 'PostController@edit');
Route::get('admin/posts/add', 'PostController@create');

Route::get('posts/{slug}/edit', [
    'uses' => 'PostController@edit',
    'middleware' => 'auth'
]);


patch('admin/posts/{slug}/edit','PostController@update');
patch('admin/posts/add','PostController@store');

Route::get('my-account', 'HomeController@account');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::post('login-success', function()
{
    die('test');
    // validate
    // process login
    // if successful, redirect
    return Redirect::intended();
});


