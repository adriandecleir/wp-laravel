<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />

    <title>Laravel</title>

    <link href="{{ asset('/css/admin.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    {!! HTML::script('js/global.js') !!}
</head>
<body>
<nav class="navbar navbar-default admin">
    <div class="container-fluid">
        <div class="navbar-header">
            <span class="wp-laravel-icon dashicons"></span>
            <a class="navbar-brand back-to-site " href="{{ url('') }}">WP Laravel</a>
            <div class="admin-logout">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                <a href="{{ url('/auth/logout') }}">Logout</a>
            </div>

            </ul>

        </div>

    </div>
</nav>
<div class="admin-sidebar">
    <div class="sidebar-menu-item posts">
        <div class="menu-image"></div>
        <a class="btn btn-default" href="{{ url('/admin/posts') }}">Posts</a>
    </div>

</div>
@yield('content')

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
