@extends('admin/app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading admin">
                        <h1>{{$post->title}}</h1>
                    </div>
                    @if(Session::has('message'))
                        <div>
                            <p class="alert alert-success success">{{Session::get('message')}}</p>
                        </div>
                    @endif
                    <div class="form-group">
                            {!! Form::open(['url' => '/admin/posts/'.$slug.'/edit', 'method' => 'PATCH'] ) !!}
                                @include('admin/posts/form')
                            {!!Form::close() !!}
                     </div>
                 </div>
            </div>
         </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-default" href="/admin/posts">Back to posts</a>
            </div>
        </div>

@endsection