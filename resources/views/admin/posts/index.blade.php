@extends('admin/app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading admin"><h1>Posts</h1></div>
                    <div class="panel-body">
                        <a class="btn btn-default" href="/admin/posts/add">Add new</a>
                        @foreach ($posts as $post)
                            <a class="btn btn-default" href="/admin/posts/{{$post->slug}}/edit">Title: {{ $post->title }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-default" href="/admin">Back to Admin</a>
            </div>
        </div>
    </div>
@endsection