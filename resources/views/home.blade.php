@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"><h1>Home</h1></div>
                @foreach ($posts as $post)
                <div class="single-post">
                    <h2>{{$post->title}}</h2>
                    <p>{{$post->content}}</p>

                </div>
                @endforeach
				<div class="panel-body">
                    This is like the main home page!!! whether logged in or not!!!
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
